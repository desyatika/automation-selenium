package com.test.qa.log;

import java.util.logging.Logger;

public class Log {
	public void log() {
		throw new IllegalStateException("logging class");
	}

	public static Logger logging = Logger.getLogger(Logger.class.getName());

	public static void info(String message) {
		logging.info(message);
	}

	public static void warn(String message) {
		logging.warning(message);
	}
}
