package com.test.qa.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.test.qa.log.Log;

public class SignUpTestCases {
	public static WebDriver driver;
	public static String baseUrl;
	
	@BeforeTest
	public static void beforeTest() {
		Log.info("=====================================================================");
		Log.info("================================Sign Up started======================");
		Log.info("=====================================================================");
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		System.setProperty("webdriver.chrome.driver", "C:\\Software\\selenium\\chromedriver\\chromedriver.exe");
		
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	@Test(description = "Sign Up with all empty field")
	public static void tc01() throws Exception {
		driver.get("https://www.amazon.com/");
		
		// click icon sign in
		driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]")).click();
		
		// click sign up
		driver.findElement(By.id("createAccountSubmit")).click();
		
		// submit with all empty field
		driver.findElement(By.id("continue")).click();
		driver.navigate().refresh();
	}
	
	@Test(description = "Sign Up with username was empty")
	public static void tc02() throws Exception {
		
		// email
		driver.findElement(By.id("ap_email")).click();
		driver.findElement(By.id("ap_email")).clear();
		driver.findElement(By.id("ap_email")).sendKeys("desyatika@gmail.com");
		
		// password
		driver.findElement(By.id("ap_password")).click();
		driver.findElement(By.id("ap_password")).clear();
		driver.findElement(By.id("ap_password")).sendKeys("@t1k@@123");
		
		// re-enter password
		driver.findElement(By.id("ap_password_check")).click();
		driver.findElement(By.id("ap_password_check")).clear();
		driver.findElement(By.id("ap_password_check")).sendKeys("@t1k@@123");

		// submit 
		driver.findElement(By.id("continue")).click();
		driver.navigate().refresh();
	}
	
	@Test(description = "Sign Up with email was empty")
	public static void tc03() throws Exception {
		
		// username
		driver.findElement(By.id("ap_customer_name")).click();
		driver.findElement(By.id("ap_customer_name")).clear();
		driver.findElement(By.id("ap_customer_name")).sendKeys("desy atika");
		
		// password
		driver.findElement(By.id("ap_password")).click();
		driver.findElement(By.id("ap_password")).clear();
		driver.findElement(By.id("ap_password")).sendKeys("@t1k@@123");
		
		// re-enter password
		driver.findElement(By.id("ap_password_check")).click();
		driver.findElement(By.id("ap_password_check")).clear();
		driver.findElement(By.id("ap_password_check")).sendKeys("@t1k@@123");

		// submit 
		driver.findElement(By.id("continue")).click();
//		driver.navigate().refresh();
	}	
	
	@AfterTest
	public static void afterTest() {
//		driver.close();
		
		Log.info("==========================================================================");
		Log.info("=============================Sign In Finished=============================");
	}
}
