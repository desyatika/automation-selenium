package com.test.qa.page;

public @interface Step {

	String value();

}
