package com.test.qa.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.test.qa.log.Log;

public class SignInTestCases {
	public static WebDriver driver;
	public static String baseUrl;
	
	@BeforeTest
	public static void beforeTest() {
		Log.info("=====================================================================");
		Log.info("================================Sign In started======================");
		Log.info("=====================================================================");
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		System.setProperty("webdriver.chrome.driver", "C:\\Software\\selenium\\chromedriver\\chromedriver.exe");
		
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	@Test(description = "Sign In with email empty")
	public static void tc01() throws Exception {
		driver.get("https://www.amazon.com/");
		
		// click icon sign in
		driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]")).click();
		
		// input invalid email
		driver.findElement(By.id("ap_email")).click();
		driver.findElement(By.id("ap_email")).clear();
		driver.findElement(By.id("ap_email")).sendKeys("");
		
		// button Continue
		driver.findElement(By.id("continue")).submit();
		driver.navigate().back();
	}
	
	@Test(description = "Sign In with email unregistered")
	public static void tc02() throws Exception {
		// click icon sign in
		driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]")).click();
		
		// input invalid email
		driver.findElement(By.id("ap_email")).click();
		driver.findElement(By.id("ap_email")).clear();
		driver.findElement(By.id("ap_email")).sendKeys("desyatika@gmail.com");
		
		// button Continue
		driver.findElement(By.id("continue")).submit();
		driver.navigate().refresh();
	}
	
	@Test(description = "Sign In with invalid email address")
	public static void tc03() throws Exception {
		// click icon sign in
//		driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]")).click();
		
		// input invalid email
		driver.findElement(By.id("ap_email")).click();
		driver.findElement(By.id("ap_email")).clear();
		driver.findElement(By.id("ap_email")).sendKeys("desyatika");
		
		// button Continue
		driver.findElement(By.id("continue")).submit();
	}
	
	@AfterTest
	public static void afterTest() {
//		driver.close();
		
		Log.info("==========================================================================");
		Log.info("=============================Sign In Finished=============================");
	}
}
